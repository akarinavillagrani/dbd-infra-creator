resource "digitalocean_database_db" "database-example" {
  cluster_id = digitalocean_database_cluster.postgres-project.id
  name       = "market"
}

resource "digitalocean_database_cluster" "postgres-project" {
  name       = "postgres-cluster-project"
  engine     = "pg"
  version    = "11"
  size       = "db-s-1vcpu-1gb"
  region     = "nyc1"
  node_count = 1
}
